pub mod render_gl;
pub mod resources;

use failure::{self, Error};
use gl;
use render_gl::data::*;
use resources::Resources;

use std::mem;
use std::path::Path;

#[macro_use]
extern crate render_gl_derive;

#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct Vertex {
    #[location = 0]
    pub position: Vec3f,
    #[location = 1]
    pub color: u2_u10_u10_u10_rev_float,
}

fn main() -> Result<(), Error> {
    let res = Resources::from_project_root(Path::new("assets"))?;

    let sdl = sdl2::init().map_err(failure::err_msg)?;
    let video_subsystem = sdl.video().map_err(failure::err_msg)?;

    let gl_attr = video_subsystem.gl_attr();

    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 1);

    let window = video_subsystem
        .window("Game", 900, 700)
        .opengl()
        .resizable()
        .build()
        .unwrap();

    let _gl_context = window.gl_create_context().map_err(failure::err_msg)?;
    let gl = gl::Gl::load_with(|s| {
        video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void
    });

    unsafe {
        gl.Viewport(0, 0, 900, 700);
        gl.ClearColor(0.3, 0.3, 0.5, 1.0);
    }

    let shader_program = render_gl::Program::from_res(&gl, &res, "shaders/triangle")?;

    shader_program.set_used();

    let mut event_pump = sdl.event_pump().map_err(failure::err_msg)?;

    let vertices: Vec<Vertex> = vec![
        Vertex {
            position: Vec3f(-0.5, -0.5, 0.0),
            color: (1.0, 0.0, 0.0, 1.0).into(),
        },
        Vertex {
            position: Vec3f(0.5, -0.5, 0.0),
            color: (0.0, 1.0, 0.0, 1.0).into(),
        },
        Vertex {
            position: Vec3f(0.0, 0.5, 0.0),
            color: (0.0, 0.0, 1.0, 1.0).into(),
        },
    ];

    let mut vbo: gl::types::GLuint = 0;
    unsafe {
        gl.GenBuffers(1, &mut vbo);
        gl.BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl.BufferData(
            gl::ARRAY_BUFFER,
            (vertices.len() * mem::size_of::<Vertex>()) as gl::types::GLsizeiptr,
            vertices.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW,
        );
        gl.BindBuffer(gl::ARRAY_BUFFER, 0);
    }
    let mut vao: gl::types::GLuint = 0;
    unsafe {
        gl.GenVertexArrays(1, &mut vao);
        gl.BindVertexArray(vao);

        gl.BindBuffer(gl::ARRAY_BUFFER, vbo);

        Vertex::vertex_attrib_pointers(&gl);

        gl.BindBuffer(gl::ARRAY_BUFFER, 0);
        gl.BindVertexArray(0);
    }

    'main: loop {
        for event in event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'main,
                _ => {}
            }
        }

        shader_program.set_used();
        unsafe {
            gl.Clear(gl::COLOR_BUFFER_BIT);
            gl.BindVertexArray(vao);
            gl.DrawArrays(gl::TRIANGLES, 0, 3);
        }

        window.gl_swap_window();
    }
    Ok(())
}
