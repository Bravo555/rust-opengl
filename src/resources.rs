use failure::Fail;
use std::ffi;
use std::fs;
use std::io::{self, prelude::*};
use std::path::{Path, PathBuf};

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "I/O error")]
    Io(#[cause] io::Error),
    #[fail(display = "File contains a null byte")]
    FileContainsNil,
    #[fail(display = "Failed to get exe path")]
    FailedToGetExePath,
}

pub struct Resources {
    root_path: PathBuf,
}

impl Resources {
    pub fn from_relative_exe_path(rel_path: &Path) -> Result<Resources, Error> {
        let exe_path = std::env::current_exe().map_err(|_| Error::FailedToGetExePath)?;
        let path = exe_path.parent().unwrap(); // safe bcs a file always has a parent (directory)

        Ok(Resources {
            root_path: path.join(rel_path),
        })
    }

    pub fn from_project_root(rel_path: &Path) -> Result<Resources, Error> {
        let project_root: PathBuf = env!("CARGO_MANIFEST_DIR").parse().unwrap();
        Ok(Resources {
            root_path: project_root.join(rel_path),
        })
    }

    pub fn load_cstring(&self, resource_name: &str) -> Result<ffi::CString, Error> {
        let mut file = fs::File::open(self.root_path.join(resource_name))?;
        let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);
        file.read_to_end(&mut buffer)?;

        if buffer.iter().find(|i| **i == 0).is_some() {
            return Err(Error::FileContainsNil);
        }

        Ok(unsafe { ffi::CString::from_vec_unchecked(buffer) })
    }
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Self {
        Error::Io(other)
    }
}
