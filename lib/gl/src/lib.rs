use std::rc::Rc;
use std::ops::Deref;

mod bindings {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

pub use bindings::*;

#[derive(Clone)]
pub struct Gl(Rc<bindings::Gl>);

impl Gl {
    pub fn load_with<F>(loadfn: F) -> Gl
    where F: FnMut(&'static str) -> *const types::GLvoid {
        Gl(Rc::new(bindings::Gl::load_with(loadfn)))
    }
}

impl Deref for Gl {
    type Target = bindings::Gl;
    fn deref(&self) -> &bindings::Gl {
        &self.0
    }
}
